$(function () {
    var windowWidth = $(window).width();
    var windowSp = 750;
    var windowPc = 1200;
    if (windowWidth < windowSp) {
      // 画面サイズが700未満の時の処理
      $(function () {
        $('.slide_flow').slick({
          infinite: false,
          mobileFirst: true,
          dots: false, //ドットのナビゲーションを表示
          slidesToShow: 1, //表示するスライドの数
          arrows: false, //前・次の矢印
          centerMode: true, //要素を中央寄せ
          centerPadding: '17%', //両サイドの見えている部分のサイズ
          focusOnSelect: true, //クリックした要素にスライドを有効化
        });
      });
    } else if (windowWidth >= windowSp && windowWidth < windowPc) {
      // 画面サイズが700以上の時の処理
      $('.slide_flow').slick('unslick');
    }
  });

  $(function(){
    // #で始まるリンクをクリックしたら実行されます
    $('a[href^="#"]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒で記述
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
    });
  });